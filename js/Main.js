// TODO: make the player force change direction while inbetween two blocks?
document.addEventListener("keydown", function(e) {
	if (is_first_play) {
		begin();
	} else if (e.code === "ArrowUp" || e.key === "z") {
		if (player.case.permissions.allowToGoUp()) {
			player.direction = "up";
		}
	} else if (e.code === "ArrowRight" || e.key === "d") {
		if (player.case.permissions.allowToGoRight()) {
			player.direction = "right";
		}
	} else if (e.code === "ArrowDown" || e.key === "s") {
		if (player.case.permissions.allowToGoDown()) {
			player.direction = "down";
		}
	} else if (e.code === "ArrowLeft" || e.key === "q") {
		if (player.case.permissions.allowToGoLeft()) {
			player.direction = "left";
		}
	}
});

is_first_play = true;

function begin() {
	if (is_first_play) {
		play();
		is_first_play = false
	} else {
		replay();
	}
}

function play() {
	document.getElementById('score').innerText = "0";
	game = setInterval(function() {
		switch (player.direction) {
			case 'up':
				player.moveUp();
				break;
			case 'right':
				player.moveRight();
				break;
			case 'down':
				player.moveDown();
				break;
			case 'left':
				player.moveLeft();
				break;
			case 'pause':
				break;
		}
		player.checkBonus();
		for (const monster of monsters) {
			if (monster.checkPlayer()) {
				return;
			}
			monster.findPlayer();
			monster.walk();
			monster.checkPlayer();
		}
	}, 300);

	map = new Map();
	player = new Player('Arnold', map.getCase(1, 1));
	monsters = [
		new Monster("Miklos", map.getCase(1, 15)),
		new Monster("Bellahsene", map.getCase(18, 1)),
		new Monster("Lazaar", map.getCase(18, 15))
	];
}

function replay() {
	map = new Map();
	
	player.case = map.getCase(1, 1);
	player.points = 0;
	player.direction = 'pause';
	player.setSprite();

	monsters[0].case = map.getCase(1, 15);
	monsters[0].direction = 'pause';
	monsters[0].setSprite();
	
	monsters[1].case = map.getCase(18, 1);
	monsters[1].direction = 'pause';
	monsters[1].setSprite();
	
	monsters[2].case = map.getCase(18, 15);
	monsters[2].direction = 'pause';
	monsters[2].setSprite();
}
