class Character {

    /**
     * Creates a Character (Monster or Player) with (x, y) position and URL to its sprite.
     * @param name The name of the Character.
     * @param _case The case where the character lies on the map.
     * @param path Path to the sprite image location.
     */
    constructor(name, _case, path) {
        this.name = name;
        this.case = _case;
        this.direction = 'pause';
        this.path = path;
        this.sprite = document.createElement('img');
        this.setSprite();
        this.sprite.style.width = '33px';
        this.sprite.style.height = '33px';
        this.sprite.style.position = 'absolute';
        this.sprite.style.transition = "0.3s linear";
        this.sprite.style.zIndex = 1;
        document.querySelector('body').appendChild(this.sprite);
    }

    setSprite() {
        this.sprite.style.top = (this.case.posY - 1) * 35 + 9 + 'px';
        this.sprite.style.left = (this.case.posX - 1) * 35 + 9 + 'px';
        switch (this.direction) {
            case 'up':
                this.sprite.src = this.path + "_up.png";
                break;
            case 'right':
                this.sprite.src = this.path + "_right.png";
                break;
            case 'down':
                this.sprite.src = this.path + "_down.png";
                break;
            case 'left':
                this.sprite.src = this.path + "_left.png";
                break;
            case 'pause':
                this.sprite.src = this.path + "_right.png";
                break;
        }
    };

    // TODO: remove moveUp and such to fully integrates into setSprite() ?

    /**
     * Move the character up.
     */
    moveUp() {
        if (this.case.permissions.allowToGoUp()) {
            this.case = map.cases[this.case.posY-2][this.case.posX-1];
            this.setSprite();
            //console.log(this.name + " moved up.");
        } else {
            //console.log(this.name + " cannot move up.");
        }
    };

    /**
     * Move the character to the right.
     */
    moveRight() {
        if (this.case.permissions.allowToGoRight()) {
            this.case = map.cases[this.case.posY-1][this.case.posX];
            this.setSprite();
            //console.log(this.name + " moved right.");
        } else {
            //console.log(this.name + " cannot move right.");
        }
    };

    /**
     * Move the character down.
     */
    moveDown() {
        if (this.case.permissions.allowToGoDown()) {
            this.case = map.cases[this.case.posY][this.case.posX-1];
            this.setSprite();
            //console.log(this.name + " moved down.");
        } else {
            //console.log(this.name + " cannot move down.");
        }
    };

    /**
     * Move the character to the left.
     */
    moveLeft() {
        if (this.case.permissions.allowToGoLeft()) {
            this.case = map.cases[this.case.posY-1][this.case.posX-2];
            this.setSprite();
            //console.log(this.name + " moved left.");
        } else {
            //console.log(this.name + " cannot move left.");
        }
    };

}