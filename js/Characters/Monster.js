class Monster extends Character {

    /**
     * Call Character.constructor() with a case (sprite is predefined).
     * @param name The name of the Monster.
     * @param _case The case on which the Monster lies on the map.
     */
    constructor(name, _case) {
        super(name, _case, "img/monster");
        this.moves = [];
        this.from = this.case;
        this.goto = null;
    }

    /**
     * Search the player. If found, save the position and follow him.
     * TODO: Vorbid through-wall vision and finish the method.
     */
    findPlayer() {
        if (this.case.posX === player.case.posX || this.case.posY === player.case.posY) { // on one (x or y) same line
            this.goto = player.case; // set goto case
        } else if (this.goto === this.case) { // if monster is on goto case and doesn't see anymore the player
            this.goto = null; // remove goto
        }
    };

    /**
     * Walk around - or - at the player if saw. Cannot go back.
     */
    walk() {
        let permissions = this.case.permissions.getPermissions();
        if (this.goto === null) { // if not found, random walk
            this.direction = permissions[Math.trunc(Math.random() * permissions.length)];
        } else {
            // TODO: find the best path to get to player.
            this.direction = permissions[Math.trunc(Math.random() * permissions.length)];
        }
        switch (this.direction) {
            case 'up':
                this.moveUp();
                break;
            case 'right':
                this.moveRight();
                break;
            case 'down':
                this.moveDown();
                break;
            case 'left':
                this.moveLeft();
                break;
            case 'pause':
                break;
        }
    };

    /**
     * Check if a player is on the case. If true, block the game.
     * @returns boolean
     */
    checkPlayer() {
        if (this.case.posX === player.case.posX && this.case.posY === player.case.posY) {
            clearInterval(game);
            return true;
        }
        return false;
    }

}