class Player extends Character {

    /**
     * Call Character.constructor() with a case (sprite is predefined).
     * @param name The name of the player.
     * @param _case The case on which the Player lies on the map.
     */
    constructor(name, _case) {
        super(name, _case, "img/player");
        this.points = 0;
    }

    checkBonus() {
        if (this.case.bonus != null) {
            this.points += this.case.bonus.points;
            this.case.bonus.removeSprite();
            this.case.removeBonus();
            document.getElementById('score').innerText = this.points;
        }
    }

}