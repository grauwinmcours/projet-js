class Case {

    /**
     * Create a Case with permissions and bonus
     * @param {int} x Horizontal value
     * @param {int} y Vertical value
     * @param {Permissions} permissions define the permissions of this case
     * @param {Bonus} bonus define the bonus in this case
     */
    constructor(x, y, permissions, bonus) {
        this.posX = x;
        this.posY = y;
        this.permissions = permissions;
        this.bonus = bonus;
        if(this.bonus != null) {
            this.bonus.setSprite(x, y);
        }
    }

    removeBonus() {
        this.bonus = null;
    }

}