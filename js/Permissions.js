class Permissions {

    /**
     * Create Permissions which describe the move possible for the Player
     * @param {0,1} pUp The permission to move to the top
     * @param {0,1} pRight The permission to move to the right
     * @param {0,1} pDown The permission to move to the bottom
     * @param {0,1} pLeft The permission to move to the left
     */
    constructor(pUp, pRight, pDown, pLeft) {
        this.pUp = pUp;
        this.pRight = pRight;
        this.pDown = pDown;
        this.pLeft = pLeft;
    }

    /**
     * Set the new value for this Permissions
     * @param {0,1} pUp The permission to move to the top
     * @param {0,1} pRight The permission to move to the right
     * @param {0,1} pDown The permission to move to the bottom
     * @param {0,1} pLeft The permission to move to the left
     */
    setPermissions(pUp, pRight, pDown, pLeft) {
        this.pUp = pUp;
        this.pRight = pRight;
        this.pDown = pDown;
        this.pLeft = pLeft;
    };

    /**
     * @return boolean 
     */
    allowToGoUp() {
        return this.pUp === 1;
    }

    /**
     * @return boolean 
     */
    allowToGoRight() {
        return this.pRight === 1;
    }

    /**
     * @return boolean 
     */
    allowToGoDown() {
        return this.pDown === 1;
    }

    /**
     * @return boolean 
     */
    allowToGoLeft() {
        return this.pLeft === 1;
    }

    /**
     * Returns an array with directions values (all are true).
     * @returns {Array}
     */
    getPermissions() {
        let result = [];
        if (this.allowToGoUp()) {
            result.push("up");
        }
        if (this.allowToGoRight()) {
            result.push("right");
        }
        if (this.allowToGoDown()) {
            result.push("down");
        }
        if (this.allowToGoLeft()) {
            result.push("left");
        }
        return result;
    };
}