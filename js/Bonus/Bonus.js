class Bonus {
    /**
     * Create a Bonus with points
     * @param {int} points The value of the bonus
     */
    constructor(points, path) {
        this.points = points;
        this.sprite = document.createElement('img');
        this.sprite.src = path + '.png';
        this.sprite.style.width = '11px';
        this.sprite.style.height = '11px';
        this.sprite.style.position = 'absolute';
        this.sprite.style.transition = "0.3s linear";
        document.querySelector('body').appendChild(this.sprite);
    }

    setSprite(posX, posY) {
        this.sprite.style.top = (posY - 1) * 35 + 20 + 'px';
        this.sprite.style.left = (posX - 1) * 35 + 20 + 'px';
    }

    removeSprite() {
        console.log('1');
        document.querySelector('body').removeChild(this.sprite);
    }
}